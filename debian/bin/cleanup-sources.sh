#! /bin/sh
# $0 --upstream-version <version> <filename>
# Based on "debian/bin/uscan_repack.sh" of the enblend-enfuse package.
# © 2007, Sebastian Harl <sh@tokkee.org> and licensed under the GPL.
# Modified for player by Daniel Hess <daniel@rio-grande.ping.de> in
# 2010.
# Modified to cleanup a unpacked source tree in 2010 by
# Michael Janssen <jamuraa@base0.net> 

echo
set -ex

# Remove Sun RPC code because of old license
for i in xdr.c xdr_array.c xdr_float.c xdr_intXX_t.c xdr_mem.c xdr_sizeof.c; do
	rm -rf "replace/$i"
done
rm -rf "replace/rpc"

# Remove because of missing licence informations
rm -rf "replace/gettimeofday.c"

for patch in debian/patches-dfsg/*.patch; do
  patch -p1 < $patch
done

